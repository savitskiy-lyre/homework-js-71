import './App.css';
import Layout from "./components/UI/Layout/Layout";
import {Switch, Route} from "react-router-dom";
import Dishes from "./containers/Dishes/Dishes";
import {useEffect} from "react";
import {useDispatch, useSelector} from "react-redux";
import {fetchDishes} from "./store/actions/menuActions";
import DishesSettings from "./containers/DishesSettings/DishesSettings";
import Orders from "./containers/Orders/Orders";
import {Box} from "@mui/material";
import {fetchOrders} from "./store/actions/ordersActions";

function App() {
  const dispatch = useDispatch();
  const loading = useSelector(state => state.menu.loading);

  useEffect(() => {
    dispatch(fetchDishes());
    dispatch(fetchOrders());
  }, [dispatch])

  return (
    <Box maxWidth={'lg'} mx={'auto'} className="App">
      <Layout navItems={[
        {label: 'Cart', path: '/cart'},
        {label: 'Dishes', path: '/dishes'},
        {label: 'Orders', path: '/orders'},
      ]}
              loading={loading}
      >
        <Switch>
          <Route path={'/cart'} component={Dishes}/>
          <Route path={'/dishes'} component={DishesSettings}/>
          <Route path={'/orders'} component={Orders}/>
          <Route exact path={'/'} component={Dishes}/>
          <Route component={() => <div>Not Found</div>}/>
        </Switch>
      </Layout>
    </Box>
  );
}

export default App;
