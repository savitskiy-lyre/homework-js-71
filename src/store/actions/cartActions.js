import {axiosApi} from "../../axiosApi";
import {ORDERS_URL} from "../../config";
import {fetchOrders} from "./ordersActions";

export const ADD_ITEM_TO_CART = 'ADD_ITEM_TO_CART';
export const REMOVE_ITEM_FROM_CART = 'REMOVE_ITEM_FROM_CART';
export const RESET_ITEM_FROM_CART = 'RESET_ITEM_FROM_CART';
export const CALCULATE_CART_SUM = 'CALCULATE_CART_SUM';
export const ORDER_DISHES_REQUEST = 'ORDER_DISHES_REQUEST';
export const ORDER_DISHES_SUCCESS = 'ORDER_DISHES_SUCCESS';
export const ORDER_DISHES_FAILURE = 'ORDER_DISHES_FAILURE';
export const SHOW_ORDER_ALERT = 'SHOW_ORDER_ALERT';


export const addItemToCart = (name) => ({type: ADD_ITEM_TO_CART, payload: name});
export const removeItemFromCart = (name) => ({type: REMOVE_ITEM_FROM_CART, payload: name});
export const resetItemFromCart = (name) => ({type: RESET_ITEM_FROM_CART, payload: name});
export const calculateCartSum = (prices) => ({type: CALCULATE_CART_SUM, payload: prices});
export const orderDishesRequest = () => ({type: ORDER_DISHES_REQUEST});
export const orderDishesSuccess = () => ({type: ORDER_DISHES_SUCCESS});
export const orderDishesFailure = (error) => ({type: ORDER_DISHES_FAILURE, payload: error});
export const showOrderAlert = (bool) => ({type: SHOW_ORDER_ALERT, payload: bool});

export const orderDishes = (order, modalClose) => {
   return async (dispatch) => {
      try {
         dispatch(orderDishesRequest());
         await axiosApi.post(ORDERS_URL, order);
         dispatch(fetchOrders());
         dispatch(orderDishesSuccess());
         dispatch(showOrderAlert(true));
         modalClose();
      } catch (error) {
         dispatch(orderDishesFailure(error));
         dispatch(showOrderAlert(false))
         modalClose();
      }

   }
}