import {axiosApi} from "../../axiosApi";
import {ORDERS_URL, REMOVE_ORDER_URL} from "../../config";

export const FETCH_ORDERS_REQUEST = 'FETCH_ORDERS_REQUEST';
export const FETCH_ORDERS_SUCCESS = 'FETCH_ORDERS_SUCCESS';
export const FETCH_ORDERS_FAILURE = 'FETCH_ORDERS_FAILURE';

export const REMOVE_ORDER_REQUEST = 'REMOVE_ORDER_REQUEST';
export const REMOVE_ORDER_SUCCESS = 'REMOVE_ORDER_SUCCESS';
export const REMOVE_ORDER_FAILURE = 'REMOVE_ORDER_FAILURE';

export const fetchOrdersRequest = () => ({type: FETCH_ORDERS_REQUEST});
export const fetchOrdersSuccess = (data) => ({type: FETCH_ORDERS_SUCCESS, payload: data});
export const fetchOrdersFailure = (error) => ({type: FETCH_ORDERS_FAILURE, payload: error});

export const fetchOrders = () => {
  return async (dispatch) => {
    try {
      dispatch(fetchOrdersRequest());
      const {data} = await axiosApi.get(ORDERS_URL);
      Object.keys(data).forEach((key) => {
        data[key].id = Math.random();
      })
      dispatch(fetchOrdersSuccess(data));
    } catch (error) {
      console.log(error);
      dispatch(fetchOrdersFailure(error));
    }
  }
}
export const removeOrderRequest = () => ({type: REMOVE_ORDER_REQUEST});
export const removeOrderSuccess = () => ({type: REMOVE_ORDER_SUCCESS});
export const removeOrderFailure = (error) => ({type: REMOVE_ORDER_FAILURE, payload: error});

export const removeOrder = (id) => {
  return async (dispatch) => {
    try {
      dispatch(removeOrderRequest());
      await axiosApi.delete(REMOVE_ORDER_URL + id + ".json");
      dispatch(fetchOrders());
      dispatch(removeOrderSuccess());
    } catch (error) {
      console.log(error);
      dispatch(removeOrderFailure(error));
    }
  }
}