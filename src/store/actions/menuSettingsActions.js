import {axiosApi} from "../../axiosApi";
import {DISHES_URL, REMOVE_DISHES_URL} from "../../config";
import {fetchDishes} from "./menuActions";

export const ADD_MENU_ITEM_REQUEST = 'ADD_MENU_ITEM_REQUEST';
export const ADD_MENU_ITEM_SUCCESS = 'ADD_MENU_ITEM_SUCCESS';
export const ADD_MENU_ITEM_FAILURE = 'ADD_MENU_ITEM_FAILURE';

export const EDIT_MENU_ITEM_REQUEST = 'EDIT_MENU_ITEM_REQUEST';
export const EDIT_MENU_ITEM_SUCCESS = 'EDIT_MENU_ITEM_SUCCESS';
export const EDIT_MENU_ITEM_FAILURE = 'EDIT_MENU_ITEM_FAILURE';

export const REMOVE_MENU_ITEM_REQUEST = 'REMOVE_MENU_ITEM_REQUEST';
export const REMOVE_MENU_ITEM_SUCCESS = 'REMOVE_MENU_ITEM_SUCCESS';
export const REMOVE_MENU_ITEM_FAILURE = 'REMOVE_MENU_ITEM_FAILURE';

export const ADD_SENT_DATA = 'ADD_SENT_DATA';

export const addSentData = (data) => ({type: ADD_SENT_DATA, payload: data});

export const addMenuItemRequest = () => ({type: ADD_MENU_ITEM_REQUEST});
export const addMenuItemSuccess = () => ({type: ADD_MENU_ITEM_SUCCESS});
export const addMenuItemFailure = (error) => ({type: ADD_MENU_ITEM_FAILURE, payload: error});

export const addMenuItem = (order, modalClose) => {
  return async (dispatch) => {
    try {
      dispatch(addMenuItemRequest());
      await axiosApi.post(DISHES_URL, order);
      dispatch(fetchDishes());
      dispatch(addMenuItemSuccess());
      modalClose();
    } catch (error) {
      dispatch(addMenuItemFailure(error));
      modalClose();
    }

  }
};

export const editMenuItemRequest = () => ({type: EDIT_MENU_ITEM_REQUEST});
export const editMenuItemSuccess = () => ({type: EDIT_MENU_ITEM_SUCCESS});
export const editMenuItemFailure = (error) => ({type: EDIT_MENU_ITEM_FAILURE, payload: error});

export const editMenuItem = (data, modalClose, key) => {
  return async (dispatch) => {
    try {
      dispatch(editMenuItemRequest());
      await axiosApi.put(`${REMOVE_DISHES_URL + key}.json`, data);
      dispatch(fetchDishes());
      dispatch(editMenuItemSuccess());
      modalClose();
    } catch (error) {
      dispatch(editMenuItemFailure(error));
      modalClose();
    }
  };
};

export const removeMenuItemRequest = () => ({type: REMOVE_MENU_ITEM_REQUEST});
export const removeMenuItemSuccess = () => ({type: REMOVE_MENU_ITEM_SUCCESS});
export const removeMenuItemFailure = (error) => ({type: REMOVE_MENU_ITEM_FAILURE, payload: error});

export const removeMenuItem = (key) => {
  return async (dispatch) => {
    try {
      dispatch(removeMenuItemRequest());
      await axiosApi.delete(`${REMOVE_DISHES_URL + key}.json`);
      dispatch(fetchDishes());
      dispatch(removeMenuItemSuccess());
    } catch (error) {
      dispatch(removeMenuItemFailure(error));
    }
  };
};