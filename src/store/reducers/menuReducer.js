import {FETCH_DISHES_FAILURE, FETCH_DISHES_REQUEST, FETCH_DISHES_SUCCESS} from "../actions/menuActions";

const initState = {
   dishesMenu: null,
   loading: false,
};
const menuReducer = (state = initState, action) => {
   switch (action.type){
      case FETCH_DISHES_REQUEST:
         return {...state, loading: true};
      case FETCH_DISHES_SUCCESS:
         return {...state, loading: false, dishesMenu: action.payload};
      case FETCH_DISHES_FAILURE:
         console.log(action.payload);
         return {...state, loading: false};
      default:
         return state;
   }

}

export default menuReducer;