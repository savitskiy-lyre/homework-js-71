import {
  ADD_MENU_ITEM_FAILURE,
  ADD_MENU_ITEM_REQUEST,
  ADD_MENU_ITEM_SUCCESS,
  ADD_SENT_DATA,
  EDIT_MENU_ITEM_FAILURE,
  EDIT_MENU_ITEM_REQUEST,
  EDIT_MENU_ITEM_SUCCESS,
  REMOVE_MENU_ITEM_FAILURE,
  REMOVE_MENU_ITEM_REQUEST,
  REMOVE_MENU_ITEM_SUCCESS
} from "../actions/menuSettingsActions";

const initState = {
  sentData: null,
  error: null,
  sendBtnLoading: false,
  currency: 'kgs',
  modalOpen: false,
};
const menuSettingsReducer = (state = initState, action) => {

  switch (action.type) {
    case ADD_MENU_ITEM_REQUEST:
      return {...state, error: null, sendBtnLoading: true};
    case ADD_MENU_ITEM_SUCCESS:
      return {...state, sentData: null, sendBtnLoading: false};
    case ADD_MENU_ITEM_FAILURE:
      return {...state, error: action.payload, sendBtnLoading: false};

    case ADD_SENT_DATA:
      return {...state, sentData: action.payload};

    case REMOVE_MENU_ITEM_REQUEST:
      return {...state, error: null, sendBtnLoading: true};
    case REMOVE_MENU_ITEM_SUCCESS:
      return {...state, sendBtnLoading: false};
    case REMOVE_MENU_ITEM_FAILURE:
      return {...state, error: action.payload, sendBtnLoading: false};

    case EDIT_MENU_ITEM_REQUEST:
      return {...state, error: null, sendBtnLoading: true};
    case EDIT_MENU_ITEM_SUCCESS:
      return {...state, sendBtnLoading: false, sentData: null};
    case EDIT_MENU_ITEM_FAILURE:
      return {...state, error: action.payload, sendBtnLoading: false};

    default:
      return state;
  }

}

export default menuSettingsReducer;