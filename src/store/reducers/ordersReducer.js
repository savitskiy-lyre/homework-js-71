import {
  FETCH_ORDERS_FAILURE,
  FETCH_ORDERS_REQUEST,
  FETCH_ORDERS_SUCCESS,
  REMOVE_ORDER_FAILURE,
  REMOVE_ORDER_REQUEST,
  REMOVE_ORDER_SUCCESS
} from "../actions/ordersActions";

const initState = {
  data: null,
  error: null,
  onDeleting: false,
};
const menuReducer = (state = initState, action) => {
  switch (action.type){
    case FETCH_ORDERS_REQUEST:
      return {...state, error: null,};
    case FETCH_ORDERS_SUCCESS:
      return {...state, data: action.payload};
    case FETCH_ORDERS_FAILURE:
      return {...state, error: action.payload};
    case REMOVE_ORDER_REQUEST:
      return {...state, error: null, onDeleting: true};
    case REMOVE_ORDER_SUCCESS:
      return {...state , onDeleting: false};
    case REMOVE_ORDER_FAILURE:
      return {...state, error: action.payload , onDeleting: false};
    default:
      return state;
  }
}

export default menuReducer;