import {
   ADD_ITEM_TO_CART,
   CALCULATE_CART_SUM, ORDER_DISHES_FAILURE, ORDER_DISHES_REQUEST, ORDER_DISHES_SUCCESS,
   REMOVE_ITEM_FROM_CART,
   RESET_ITEM_FROM_CART, SHOW_ORDER_ALERT
} from "../actions/cartActions";

const initState = {
   cartBasket: {},
   deliveryPrice: 150,
   cartSum: 0,
   sendBtnLoading: false,
   isAlertShown: null,

};
const cartReducer = (state = initState, action) => {

   switch (action.type) {
      case ADD_ITEM_TO_CART:
         let orderCount;
         orderCount = !state.cartBasket[action.payload] ? 1 : state.cartBasket[action.payload] + 1;
         return {...state, cartBasket: {...state.cartBasket, [action.payload]: orderCount}}

      case REMOVE_ITEM_FROM_CART:
         const newState = {...state.cartBasket};
         if (state.cartBasket[action.payload] === 1) {
            delete newState[action.payload];
         } else {
            newState[action.payload] = newState[action.payload] - 1;
         }
         return {...state, cartBasket: newState};

      case RESET_ITEM_FROM_CART:
         const resetItemState = {...state.cartBasket};
         delete resetItemState[action.payload];
         return {...state, cartBasket: resetItemState};

      case CALCULATE_CART_SUM:
         let prices = 0;
        for (const key in action.payload){
           if (action.payload.hasOwnProperty(key) && state.cartBasket[action.payload[key]['name']]){
              prices += state.cartBasket[action.payload[key]['name']] * action.payload[key]['price'];
           }
        }
        if (prices !== 0) prices += state.deliveryPrice;
         return {...state, cartSum: prices};
      case ORDER_DISHES_REQUEST:
         return {...state, loading: true};

      case ORDER_DISHES_SUCCESS:
         return {...state, loading: false, cartBasket: initState.cartBasket};

      case ORDER_DISHES_FAILURE:
         console.log(action.payload);
         return {...state, loading: false, cartBasket: initState.cartBasket};

      case SHOW_ORDER_ALERT:
         return {...state, loading: false, isAlertShown: action.payload};

      default:
         return state;
   }

}

export default cartReducer;