import React, {useEffect, useState} from 'react';
import CartOrder from "../../components/CartOrder/CartOrder";
import {useDispatch, useSelector} from "react-redux";
import {
  addItemToCart,
  calculateCartSum,
  orderDishes,
  removeItemFromCart,
  resetItemFromCart
} from "../../store/actions/cartActions";
import {Box} from "@mui/material";
import MenuOrder from "../../components/MenuOrder/MenuOrder";

const Dishes = () => {
  const dispatch = useDispatch();
  const dishesBasket = useSelector(state => state.cart.cartBasket);
  const dishesMenu = useSelector(state => state.menu.dishesMenu);
  const sendBtnLoading = useSelector(state => state.cart.sendBtnLoading);
  const cartSum = useSelector(state => state.cart.cartSum);
  const deliveryPrice = useSelector(state => state.cart.deliveryPrice);
  const [orderModalIsOpen, setOrderModalIsOpen] = useState(false);

  useEffect(() => {
    if (dishesMenu) {
      dispatch(calculateCartSum(dishesMenu));
    }
  }, [dispatch, dishesBasket, dishesMenu])

  return (
    <Box>

      {dishesMenu && (
        <>
          <MenuOrder dishesMenu={dishesMenu} addItem={(name) => dispatch(addItemToCart(name))}/>
          <CartOrder
            basket={{
              data: dishesBasket,
              resetItem: (key) => dispatch(resetItemFromCart(key)),
              removeItem: (key) => dispatch(removeItemFromCart(key)),
            }}
            modal={{
              open: orderModalIsOpen,
              closeHandler: () => setOrderModalIsOpen(false),
            }}
            form={{
              sentData: dishesBasket,
              sendForm: (order) => dispatch(orderDishes(order, () => setOrderModalIsOpen(false))),
              sendBtnLoading: sendBtnLoading,
            }}
            results={{
              deliveryPrice,
              cartSum,
              openModal: () => setOrderModalIsOpen(true),
            }}
          />
        </>
      )}
    </Box>
  );
};

export default Dishes;