import React, {useCallback, useMemo} from 'react';
import {Grid, Paper} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import {styled} from "@mui/material/styles";
import {useDispatch, useSelector} from "react-redux";
import {removeOrder} from "../../store/actions/ordersActions";

const Item = styled(Paper)(({theme}) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.primary,
}));

const Orders = () => {
  const dispatch = useDispatch();
  const onDeleting = useSelector((state) => state.orders.onDeleting);
  const dishes = useSelector((state) => state.menu.dishesMenu);
  const orders = useSelector((state) => state.orders.data);
  const deliveryPrice = useSelector(state => state.cart.deliveryPrice)

  const MenuPrices = useMemo(() => {
    if (dishes) {
      const prices = {};
      Object.keys(dishes).forEach(key => {
        prices[dishes[key]['name']] = parseInt(dishes[key]['price']);
      })
      return prices;
    }
  }, [dishes]);

  const calculateOrderSum = useCallback((key) => {
    let sum = 0;
    Object.keys(orders[key].data).forEach((name) => {
      sum += MenuPrices[name] * parseInt(orders[key]['data'][name])
    })
    sum += deliveryPrice;
    return sum;
  }, [MenuPrices, deliveryPrice, orders])

  return orders && dishes && (
    <>
      {Object.keys(orders).map((key) => {
        return (
          <Grid container mb={2} sx={{border: '1px solid gainsboro'}} key={orders[key].id}>
            <Grid item xs={8}>
              {Object.keys(orders[key].data).map((name) => {
                return (
                  <Grid item container key={orders[key].id + name}>
                    <Grid item xs={8}>
                      <Item elevation={0}>{orders[key]['data'][name]}x {name}</Item>
                    </Grid>
                    <Grid item xs={4}>
                      <Item elevation={0}>
                        <strong>{MenuPrices[name] * parseInt(orders[key]['data'][name])}</strong> KGS
                      </Item>
                    </Grid>
                  </Grid>
                )
              }).reverse()}
              <Grid item container>
                <Grid item xs={8}>
                  <Item elevation={0}>Delivery: </Item>
                </Grid>
                <Grid item xs={4}>
                  <Item elevation={0}><strong>{deliveryPrice}</strong> KGS</Item>
                </Grid>
              </Grid>
            </Grid>
            <Grid item xs={4} alignSelf={"flex-end"} container direction={"column"}>
              <Grid item textAlign={"center"}>
                <LoadingButton
                  onClick={() => dispatch(removeOrder(key))} loading={onDeleting}
                >
                  Complete order
                </LoadingButton>
              </Grid>
              <Grid item>
                <Item elevation={0}>Orders Total:</Item>
              </Grid>
              <Grid item>
                <Item elevation={0}><strong>{calculateOrderSum(key)}</strong> KGS</Item>
              </Grid>
            </Grid>
          </Grid>
        );
      })}
    </>
  );
};

export default Orders;