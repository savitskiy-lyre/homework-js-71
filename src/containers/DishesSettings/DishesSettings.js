import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {Grid} from "@mui/material";
import MenuSettingsItem from "../../components/MenuSettingsItem/MenuSettingsItem";
import Button from "@mui/material/Button";
import ModalOrder from "../../components/CartOrder/ModalOrder/ModalOrder";
import FormMenuItemEdit from "../../components/FormMenuItemEdit/FormMenuItemEdit";
import {addMenuItem, addSentData, editMenuItem, removeMenuItem} from "../../store/actions/menuSettingsActions";

const DishesSettings = () => {
  const dispatch = useDispatch();
  const dishesMenu = useSelector(state => state.menu.dishesMenu);
  const currency = useSelector(state => state.menuSettings.currency);
  const sendBtnLoading = useSelector(state => state.menuSettings.sendBtnLoading);
  const sentData = useSelector(state => state.menuSettings.sentData);
  const [modalAddItemOpen, setModalAddItemOpen] = useState(false);

  return dishesMenu && (
    <Grid maxWidth={'md'} mx={'auto'}>
      <Grid mb={2} container justifyContent={"flex-end"}>
        <Button
          color={"warning"}
          variant={"contained"}
          onClick={() => setModalAddItemOpen(true)}
        >Add new dishe</Button>
      </Grid>
      <Grid container direction={"column"}>
        {Object.keys(dishesMenu).map((key) => {
          return (
            <MenuSettingsItem
              key={dishesMenu[key]['id']}
              name={dishesMenu[key]['name']}
              imgUrl={dishesMenu[key]['img']}
              price={dishesMenu[key]['price']}
              currency={dishesMenu[key]['currency']}
              removeItem={() => dispatch(removeMenuItem(key))}
              editItem={() => {
                dispatch(addSentData({
                  name: dishesMenu[key]['name'],
                  url: dishesMenu[key]['img'],
                  price: dishesMenu[key]['price'],
                  key: key,
                }))
                setModalAddItemOpen(true)
              }}
            />
          );
        })}
      </Grid>
      <ModalOrder
        modal={{
          open: modalAddItemOpen,
          closeHandler: () => {
            setModalAddItemOpen(false)
            dispatch(addSentData(null))
          },
        }}
      >
        <FormMenuItemEdit
          form={{
            sentData: sentData,
            sendForm: (order) => dispatch(addMenuItem(order, () => setModalAddItemOpen(false))),
            editMenuItem: (order) => dispatch(editMenuItem(order, () => setModalAddItemOpen(false), sentData.key)),
            sendBtnLoading: sendBtnLoading,
            currency: currency,
          }}
        />
      </ModalOrder>
    </Grid>
  );
};

export default DishesSettings;