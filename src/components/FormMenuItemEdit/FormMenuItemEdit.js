import React, {useState} from 'react';
import {Grid, TextField, Typography} from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import SendIcon from "@mui/icons-material/Send";

const FormMenuItemEdit = ({form}) => {

  const [nameInpValue, setNameInpValue] = useState(() => form.sentData ? form.sentData.name :'');
  const [priceInpValue, setPriceInpValue] = useState(() => form.sentData ? form.sentData.price :'');
  const [imgUrlInpValue, setImgUrlInpValue] = useState(() => form.sentData ? form.sentData.url :'');

  const submitOrder = (e) => {
    e.preventDefault();
    if (form.sentData){
      form.editMenuItem({
        name: nameInpValue,
        img: imgUrlInpValue,
        price: priceInpValue,
        currency: form.currency,
      });
    } else {
      form.sendForm({
        name: nameInpValue,
        img: imgUrlInpValue,
        price: priceInpValue,
        currency: form.currency,
      });
    }

  }
  return (
    <form onSubmit={submitOrder}>
      <Grid container>
        <Grid item mb={4}>
          <Typography variant={'h5'}>
            Please fill in the form fields
          </Typography>
        </Grid>
        <Grid item xs={12} mb={1}>
          <TextField
            value={nameInpValue}
            onChange={e => setNameInpValue(e.target.value)}
            required
            fullWidth
            label="Name"
          />
        </Grid>
        <Grid item xs={12} mb={1}>
          <TextField
            value={priceInpValue}
            onChange={e => setPriceInpValue(e.target.value)}
            required
            fullWidth
            label="Price"
          />
        </Grid>
        <Grid item xs={12} mb={4}>
          <TextField
            value={imgUrlInpValue}
            onChange={e => setImgUrlInpValue(e.target.value)}
            required
            fullWidth
            label="Image's URL"
          />
        </Grid>
        <Grid item textAlign={"center"} xs={12}>
          <LoadingButton
            type={"submit"}
            endIcon={<SendIcon />}
            loading={form.sendBtnLoading}
            loadingPosition="end"
            variant="contained"
          >
            Send
          </LoadingButton>
        </Grid>
      </Grid>
    </form>
  );
};

export default FormMenuItemEdit;