import React, {useState} from 'react';
import {Grid, Paper} from "@mui/material";
import CardMedia from "@mui/material/CardMedia";
import Button from "@mui/material/Button";
import {styled} from "@mui/material/styles";
import LoadingButton from "@mui/lab/LoadingButton";

const Item = styled(Paper)(({theme}) => ({
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: 'center',
  color: theme.palette.text.primary,
}));

const MenuSettingsItem = (props) => {
  const [removeBtnLoading, setRemoveBtnLoading] = useState(false);

  return (
    <Grid
      container
      justifyContent={"space-between"}
      alignItems={"center"}
      mb={2}
      sx={{border: '1px solid gainsboro'}}
    >
      <Grid item mr={1}>
        <CardMedia
          component="img"
          height="64"
          image={props.imgUrl}
          alt={props.name}
        />
      </Grid>
      <Grid item mr={1} xs={4}>
        <Item elevation={0}>{props.name}</Item>
      </Grid>
      <Grid item mr={1}>
        <Item elevation={0}><strong>{props.price}</strong><span
          style={{marginLeft: '10px'}}>{props.currency}</span></Item>
      </Grid>
      <Grid item>
        <Button
          onClick={() => props.editItem()}
        >
          Edit
        </Button>
      </Grid>
      <Grid item mr={1}>
        <LoadingButton
          loading={removeBtnLoading}
          onClick={() => {
            setRemoveBtnLoading(true);
            props.removeItem();
          }}
        >
          Delete
        </LoadingButton>
      </Grid>
    </Grid>
  );
};

export default MenuSettingsItem;