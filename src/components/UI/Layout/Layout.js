import React from 'react';
import NavTabs from "../NavTabs/NavTabs";
import {Box, Grid} from "@mui/material";
import BackdropPreloader from "../BackdropPreloader/BackdropPreloader";

const Layout = ({children, navItems, loading}) => {

   return (
     <>
        <Grid container direction={"column"} sx={{borderBottom: '1px solid gainsboro'}} pb={2}>
           <Grid item alignSelf={"flex-end"}>
              <NavTabs
                items={navItems}/>
           </Grid>
        </Grid>
        <Box p={2}>
           {children}
        </Box>
       {loading && (
         <BackdropPreloader option={{type: Math.floor(Math.random() * 10)}}/>
       )}
     </>
   );
};

export default Layout;