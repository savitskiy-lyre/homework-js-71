import React, {useState} from 'react';
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import {Link, useLocation} from "react-router-dom";

const NavTabs = ({items}) => {
  const location = useLocation();

  const [value, setValue] = useState(() => {
    if (location.pathname === '/dishes') return 1
    if (location.pathname === '/orders') return 2
    return 0
  });
   const onChange = (e, value) => setValue(value);

   return (
     <Tabs value={value}
           onChange={onChange}
           textColor="secondary"
           indicatorColor="secondary"
     >
        {items.map((item) => {
           return (
                <Tab key={item.path} label={item.label} to={item.path} component={Link}/>
           );
        })}
     </Tabs>
   );
};

export default NavTabs;