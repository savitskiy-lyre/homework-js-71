import React, {useState} from 'react';
import {Grid, TextField, Typography} from "@mui/material";
import LoadingButton from '@mui/lab/LoadingButton';
import SendIcon from '@mui/icons-material/Send';

const FormOrder = ({form}) => {
   const [nameInpValue, setNameInpValue] = useState('');
   const [telInpValue, setTelInpValue] = useState('');
   const [addressInpValue, setAddressInpValue] = useState('');

   const submitOrder = (e) => {
      e.preventDefault();
      form.sendForm({
         data: form.sentData,
         profile: {
            name: nameInpValue,
            address: addressInpValue,
            tel: telInpValue,
         },
         time: new Date(),
      });
   }
   return (
     <form onSubmit={submitOrder}>
        <Grid container>
           <Grid item mb={4}>
              <Typography variant={'h5'}>
                 Please fill in the form fields
              </Typography>
           </Grid>
           <Grid item xs={12} mb={1}>
              <TextField
                value={nameInpValue}
                onChange={e => setNameInpValue(e.target.value)}
                required
                fullWidth
                label="Name"
              />
           </Grid>
           <Grid item xs={12} mb={1}>
              <TextField
                value={telInpValue}
                onChange={e => setTelInpValue(e.target.value)}
                required
                fullWidth
                label="Tel"
              />
           </Grid>
           <Grid item xs={12} mb={4}>
              <TextField
                value={addressInpValue}
                onChange={e => setAddressInpValue(e.target.value)}
                required
                fullWidth
                label="Address"
              />
           </Grid>
           <Grid item textAlign={"center"} xs={12}>
              <LoadingButton
                type={"submit"}
                endIcon={<SendIcon />}
                loading={form.sendBtnLoading}
                loadingPosition="end"
                variant="contained"
              >
                 Send
              </LoadingButton>
           </Grid>
        </Grid>
     </form>
   );
};

export default FormOrder;