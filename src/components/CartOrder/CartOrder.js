import React from 'react';
import {Grid} from "@mui/material";
import CartResults from "./CartResults/CartResults";
import CartBasket from "./CartBasket/CartBasket";


const CartOrder = ({basket, results, form, modal}) => {

   return (

     <Grid container direction={"column"} justifyContent={"space-between"}>
        <Grid item>
           <CartBasket basket={basket}/>
        </Grid>
        <Grid item>
           <CartResults results={results}
                        form={form}
                        modal={modal}
           />
        </Grid>
     </Grid>
   );
};

export default CartOrder;

