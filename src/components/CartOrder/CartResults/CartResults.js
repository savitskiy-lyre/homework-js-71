import React from 'react';
import {Grid} from "@mui/material";
import Button from "@mui/material/Button";
import AddShoppingCartIcon from "@mui/icons-material/AddShoppingCart";
import ModalOrder from "../ModalOrder/ModalOrder";
import FormOrder from "../FormOrder/FormOrder";

const CartResults = ({results, form, modal}) => {

   return (
     <Grid container direction={"column"} sx={{borderTop: '2px solid gainsboro'}}>
        <Grid item mt={2}>
           Доставка: {results.deliveryPrice} KGS
        </Grid>
        <Grid item mt={1}>
           Итого: {results.cartSum} KGS
        </Grid>
        <Grid item textAlign={"center"} my={3}>
           <Button
             onClick={results.openModal}
             variant={'contained'}
             color="success"
             disabled={results.cartSum === 0}
             aria-label="add to shopping cart"
             size={"large"}>
              Place order
              <AddShoppingCartIcon sx={{marginLeft: '10px'}}/>
           </Button>
           <ModalOrder modal={modal}>
              <FormOrder
                form={form}
              />
           </ModalOrder>
        </Grid>
     </Grid>
   );
};

export default CartResults;
