import React from 'react';
import {Backdrop, Box, Fade, Modal} from "@mui/material";

const style = {
   position: 'absolute',
   top: '50%',
   left: '50%',
   transform: 'translate(-50%, -50%)',
   width: 400,
   bgcolor: 'background.paper',
   border: '2px solid #000',
   boxShadow: 24,
   p: 4,
};

const ModalOrder = ({modal, children}) => {
   return (
     <Modal
       aria-labelledby="transition-modal-title"
       aria-describedby="transition-modal-description"
       open={modal.open}
       onClose={modal.closeHandler}
       closeAfterTransition
       BackdropComponent={Backdrop}
       BackdropProps={{
          timeout: 500,
       }}
     >
        <Fade in={modal.open}>
           <Box sx={style}>
              {children}
           </Box>
        </Fade>
     </Modal>
   );
};

export default ModalOrder;