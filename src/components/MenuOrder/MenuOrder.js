import React from 'react';
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import Typography from "@mui/material/Typography";
import CardMedia from "@mui/material/CardMedia";
import CardActions from "@mui/material/CardActions";
import Button from "@mui/material/Button";
import './MenuOrder.css';

const MenuOrder = ({dishesMenu, addItem}) => {

   return (
     <fieldset className="food-menu-item-menu">
        <legend>Menu</legend>
        {Object.keys(dishesMenu).map((key) => {
             return (
               <Card sx={{maxWidth: '300px', margin: '10px'}} key={dishesMenu[key].id}>
                  <CardContent>
                     <Typography gutterBottom variant="h6" component="div">
                        {dishesMenu[key].name}
                     </Typography>
                  </CardContent>
                  <CardMedia
                    component="img"
                    height="220"
                    image={dishesMenu[key].img}
                    alt={dishesMenu[key].name}
                  />
                  <CardActions sx={{justifyContent: 'space-evenly', margin: '10px 0'}}>
                     <Typography variant="body2" color="text.secondary" m={1} textAlign='right' fontSize={25}>
                        {dishesMenu[key].price + ' ' + dishesMenu[key].currency.toUpperCase()}
                     </Typography>
                     <Button
                       variant={'outlined'}
                       size={"large"}
                       color="primary"
                       onClick={() => addItem(dishesMenu[key].name)}
                     >
                        Add to cart
                     </Button>
                  </CardActions>
               </Card>
             )
          }
        )}
     </fieldset>
   );
};

export default MenuOrder;