export const BASE_URL = 'https://tutorial-sample-posts-blog-default-rtdb.firebaseio.com/h71';
export const DISHES_URL = '/dishes.json';
export const REMOVE_DISHES_URL = '/dishes/';
export const ORDERS_URL = '/orders.json';
export const REMOVE_ORDER_URL = '/orders/';