import axios from 'axios';
import {BASE_URL} from "./config";

export const axiosApi = axios.create({
     baseURL: BASE_URL,
});